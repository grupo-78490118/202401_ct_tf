#Se generan los archivos necesarios de ANTLR con el comando
# antlr4 -Dlanguage=Python3 turing.g4
#antlr4 -visitor -Dlanguage=Python3  turing.g4 

import llvmlite.ir as ir
import llvmlite.binding as llvm
from antlr4 import *
from turingLexer import turingLexer
from turingParser import turingParser
from turingVisitor import turingVisitor
from graphviz import Digraph
from llvmlite.ir import types
from ctypes import CFUNCTYPE, c_int, c_float

class LLVMGenerator(turingVisitor):
    def __init__(self):
        self.module = ir.Module(name="turingMachineModule")
        self.builder = None
        self.func = None
        self.tape_ptr = None
        self.state_ptr = None
        self.states = {}
        self.head = None
        self.blank = None
        self.head = None
        self.start_state = None
        self.tape_size = None
        self.input_str=None
        self.state_map = {}

    def visitTm(self, ctx:turingParser.TmContext):
        self.visit(ctx.input_block())
        self.visit(ctx.blank_block())
        self.visit(ctx.start_state_block())
        self.visit(ctx.table_block())
        self.visitChildren(ctx)

        func_type = ir.FunctionType(ir.IntType(32), [])
        self.function = ir.Function(self.module, func_type, name="main")
        block = self.function.append_basic_block(name="entry")
        self.builder = ir.IRBuilder(block)

        self.tape_size = len(ctx.input_block().STRING().getText().strip("'"))
        tape_type = ir.ArrayType(ir.IntType(8), self.tape_size)
        self.tape_ptr = ir.GlobalVariable(self.module, tape_type, name="tape")
        self.head = self.builder.alloca(ir.IntType(32), name="head")
        self.builder.store(ir.Constant(ir.IntType(32), 1), self.head)

         
        
        tape_init_values = [ord(self.blank)] + [ord(c) for c in self.input_str] + [ord(self.blank)]
        int32_ty = ir.IntType(32)
        array_ty = ir.ArrayType(int32_ty, len(tape_init_values))
        tape_init = ir.Constant.literal_array([ir.Constant(int32_ty, x) for x in tape_init_values])
        self.tape = self.builder.alloca(array_ty, name="tape")
        self.builder.store(tape_init, self.tape)
        # tape_init_values = [ord(self.blank)] + [ord(c) for c in self.input_str] + [ord(self.blank)]
        # tape_init = ir.Constant.array(ir.IntType(32), tape_init_values)
        # self.tape = self.builder.alloca(ir.ArrayType(ir.IntType(32), len(tape_init_values)), name="tape")
        # self.builder.store(tape_init, self.tape)        

        state_type = ir.IntType(8)
        self.state_ptr = ir.GlobalVariable(self.module, state_type, name="state")
        self.state_ptr.initializer = ir.Constant(state_type, ord(str(self.state_map[self.start_state])))

        # self.build_main_function()
        # self.graph()
        # self.build()

        return self.module

    def visitInput_block(self, ctx:turingParser.Input_blockContext):
        self.input_str = ctx.STRING().getText().strip("'")

    def visitBlank_block(self, ctx:turingParser.Blank_blockContext):
        self.blank = ctx.SYMBOL().getText().strip("'")

    def visitStart_state_block(self, ctx:turingParser.Start_state_blockContext):
        self.start_state = ctx.ID().getText()
        self.state_map[self.start_state] = 0  # map start state to 0

    def visitTable_block(self, ctx:turingParser.Table_blockContext):
        state_id=1
        for state in ctx.state_definition():
            state_name = state.ID().getText()
            self.state_map[state_name] = state_id
            self.states[state_name]={}
            state_id += 1
            for transition in state.transition():
                actions=transition.action()
                if (transition.symbol_list() is not None):
                    for symbol in transition.symbol_list().SYMBOL():
                        self.states[state_name][symbol.getText()] = actions
                        print("Symbol: "),symbol.getText()
                elif(transition.SYMBOL()is not None):
                        self.states[state_name][symbol.getText()] = actions
                        print("Symbol: "),symbol.getText()
            self.visit(state)
            
            
            

    def visitState(self, ctx:turingParser.State_definitionContext):
        state_name = ctx.ID().getText()
        transitions = []
        for transition in ctx.transition():
            transitions.append(self.visit(transition))
        self.states[state_name] = transitions

    def visitTransition(self, ctx:turingParser.TransitionContext):
        symbols = []
        if ctx.symbol_list():
            for symbol in ctx.symbol_list().SYMBOL():
                symbols.append(symbol.getText().strip("'"))
        else:
            symbols.append(ctx.SYMBOL().getText().strip("'"))
        action = self.visit(ctx.action())
        return (symbols, action)

    def visitAction(self, ctx:turingParser.ActionContext):
        if ctx.move_action():
            return self.visit(ctx.move_action())
        elif ctx.write_action() and ctx.state_transition():
            write = self.visit(ctx.write_action())
            transition = self.visit(ctx.state_transition())
            return ('WRITE_AND_TRANSITION', write, transition)
        elif ctx.write_action():
            return ('WRITE', self.visit(ctx.write_action()))
        elif ctx.state_transition():
            return ('TRANSITION', self.visit(ctx.state_transition()))

    def visitMove_action(self, ctx:turingParser.Move_actionContext):
        return ('MOVE', ctx.getText())

    def visitWrite_action(self, ctx:turingParser.Write_actionContext):
        return ctx.SYMBOL().getText().strip("'")

    def visitState_transition(self, ctx:turingParser.State_transitionContext):
        move = self.visit(ctx.move_action())
        if ctx.ID():
            return (move, ctx.ID().getText())
        else:
            return (move, None)

    # def build_main_function(self):
    #     func_type = ir.FunctionType(ir.VoidType(), [])
    #     self.func = ir.Function(self.module, func_type, name="main")
    #     block = self.func.append_basic_block(name="entry")
    #     self.builder = ir.IRBuilder(block)

    #     # Initialize tape with input
    #     for i, char in enumerate(self.input):
    #         ptr = self.builder.gep(self.tape_ptr, [ir.Constant(types.IntType(32), 0), ir.Constant(types.IntType(32), i)])
    #         self.builder.store(ir.Constant(ir.IntType(8), ord(char)), ptr)

    #     tape_pos = self.builder.alloca(types.IntType(32), name="tape_pos")
    #     self.builder.store(ir.Constant(types.IntType(32), 0), tape_pos)

    #     current_state = self.builder.load(self.state_ptr, name="current_state")

        


        self.builder.ret_void()

    def create_default_block(self):
        block = self.func.append_basic_block(name="default")
        self.builder.position_at_end(block)
        self.builder.ret_void()
        return block

    def handle_transitions(self, transitions, tape_pos):
        for symbols, action in transitions:
            for symbol in symbols:
                current_pos = self.builder.load(tape_pos, name="current_pos")
                tape_ptr = self.builder.gep(self.tape_ptr, [ir.Constant(types.IntType(32), 0), current_pos])
                current_symbol = self.builder.load(tape_ptr, name="current_symbol")

                with self.builder.if_else(self.builder.icmp_unsigned('==', current_symbol, ir.Constant(ir.IntType(8), ord(symbol)))) as (then, otherwise):
                    with then:
                        if action[0] == 'MOVE':
                            self.handle_move_action(action, tape_pos)
                        elif action[0] == 'WRITE':
                            self.handle_write_action(action[1], tape_ptr)
                        elif action[0] == 'TRANSITION':
                            self.handle_state_transition(action, tape_pos)
                        elif action[0] == 'WRITE_AND_TRANSITION':
                            self.handle_write_action(action[1], tape_ptr)
                            self.handle_state_transition(action[2], tape_pos)

    def handle_move_action(self, action, tape_pos):
        current_pos = self.builder.load(tape_pos, name="current_pos")
        if action[1] == 'R':
            new_pos = self.builder.add(current_pos, ir.Constant(types.IntType(32), 1))
        else:
            new_pos = self.builder.sub(current_pos, ir.Constant(types.IntType(32), 1))
        self.builder.store(new_pos, tape_pos)

    def handle_write_action(self, symbol, tape_ptr):
        self.builder.store(ir.Constant(ir.IntType(8), ord(symbol)), tape_ptr)

    def handle_state_transition(self, action, tape_pos):
        move, next_state = action
        self.handle_move_action(move, tape_pos)
        if next_state:
            self.builder.store(ir.Constant(ir.IntType(8), ord(next_state[0])), self.state_ptr)
        else:
            self.builder.ret_void()

def compile_turing_machine(file_path):
    input_stream=FileStream(file_path)
    lexer = turingLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = turingParser(stream)
    tree = parser.tm()

    llvm_gen = LLVMGenerator()
    llvm_module = llvm_gen.visit(tree)

    llvm.initialize()
    llvm.initialize_native_target()
    llvm.initialize_native_asmprinter()

    target = llvm.Target.from_default_triple()
    target_machine = target.create_target_machine()

    llvm_module_ref = llvm.parse_assembly(str(llvm_module))

    entry = engine.get_function_address('main')
    cfunc = CFUNCTYPE(ctypes.c_int32)(entry)
    
    print(f"Function address: {entry}")
    print(f"Callable function: {cfunc}")
    
    result = cfunc()
    print(f"output: {result}")

compile_turing_machine('Grupo 7/input.txt')