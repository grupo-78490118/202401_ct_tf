// Generated from d://Documentos//Universidad//2024-1//TC//TF//grupo 7//202401_ct_tf//Grupo 7//turing.g4 by ANTLR 4.13.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class turingParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, STRING=15, SYMBOL=16, 
		INT=17, ID=18, WS=19;
	public static final int
		RULE_tm = 0, RULE_symbol_list = 1, RULE_input_block = 2, RULE_blank_block = 3, 
		RULE_start_state_block = 4, RULE_table_block = 5, RULE_state_definition = 6, 
		RULE_transition = 7, RULE_action = 8, RULE_move_action = 9, RULE_write_action = 10, 
		RULE_state_transition = 11;
	private static String[] makeRuleNames() {
		return new String[] {
			"tm", "symbol_list", "input_block", "blank_block", "start_state_block", 
			"table_block", "state_definition", "transition", "action", "move_action", 
			"write_action", "state_transition"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'['", "','", "']'", "'input'", "':'", "';'", "'blank'", "'start_state'", 
			"'table'", "'{'", "'}'", "'L'", "'R'", "'write'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, "STRING", "SYMBOL", "INT", "ID", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "turing.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public turingParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TmContext extends ParserRuleContext {
		public Input_blockContext input_block() {
			return getRuleContext(Input_blockContext.class,0);
		}
		public Blank_blockContext blank_block() {
			return getRuleContext(Blank_blockContext.class,0);
		}
		public Start_state_blockContext start_state_block() {
			return getRuleContext(Start_state_blockContext.class,0);
		}
		public Table_blockContext table_block() {
			return getRuleContext(Table_blockContext.class,0);
		}
		public TerminalNode EOF() { return getToken(turingParser.EOF, 0); }
		public TmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tm; }
	}

	public final TmContext tm() throws RecognitionException {
		TmContext _localctx = new TmContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_tm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(24);
			input_block();
			setState(25);
			blank_block();
			setState(26);
			start_state_block();
			setState(27);
			table_block();
			setState(28);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Symbol_listContext extends ParserRuleContext {
		public List<TerminalNode> SYMBOL() { return getTokens(turingParser.SYMBOL); }
		public TerminalNode SYMBOL(int i) {
			return getToken(turingParser.SYMBOL, i);
		}
		public Symbol_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbol_list; }
	}

	public final Symbol_listContext symbol_list() throws RecognitionException {
		Symbol_listContext _localctx = new Symbol_listContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_symbol_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30);
			match(T__0);
			setState(31);
			match(SYMBOL);
			setState(36);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(32);
				match(T__1);
				setState(33);
				match(SYMBOL);
				}
				}
				setState(38);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(39);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Input_blockContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(turingParser.STRING, 0); }
		public Input_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_input_block; }
	}

	public final Input_blockContext input_block() throws RecognitionException {
		Input_blockContext _localctx = new Input_blockContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_input_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			match(T__3);
			setState(42);
			match(T__4);
			setState(43);
			match(STRING);
			setState(44);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Blank_blockContext extends ParserRuleContext {
		public TerminalNode SYMBOL() { return getToken(turingParser.SYMBOL, 0); }
		public Blank_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blank_block; }
	}

	public final Blank_blockContext blank_block() throws RecognitionException {
		Blank_blockContext _localctx = new Blank_blockContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_blank_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(T__6);
			setState(47);
			match(T__4);
			setState(48);
			match(SYMBOL);
			setState(49);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Start_state_blockContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(turingParser.ID, 0); }
		public Start_state_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start_state_block; }
	}

	public final Start_state_blockContext start_state_block() throws RecognitionException {
		Start_state_blockContext _localctx = new Start_state_blockContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_start_state_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			match(T__7);
			setState(52);
			match(T__4);
			setState(53);
			match(ID);
			setState(54);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Table_blockContext extends ParserRuleContext {
		public List<State_definitionContext> state_definition() {
			return getRuleContexts(State_definitionContext.class);
		}
		public State_definitionContext state_definition(int i) {
			return getRuleContext(State_definitionContext.class,i);
		}
		public Table_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_block; }
	}

	public final Table_blockContext table_block() throws RecognitionException {
		Table_blockContext _localctx = new Table_blockContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_table_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(T__8);
			setState(57);
			match(T__4);
			setState(58);
			match(T__9);
			setState(62);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(59);
				state_definition();
				}
				}
				setState(64);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(65);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class State_definitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(turingParser.ID, 0); }
		public List<TransitionContext> transition() {
			return getRuleContexts(TransitionContext.class);
		}
		public TransitionContext transition(int i) {
			return getRuleContext(TransitionContext.class,i);
		}
		public State_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_state_definition; }
	}

	public final State_definitionContext state_definition() throws RecognitionException {
		State_definitionContext _localctx = new State_definitionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_state_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			match(ID);
			setState(68);
			match(T__4);
			setState(69);
			match(T__9);
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0 || _la==SYMBOL) {
				{
				{
				setState(70);
				transition();
				}
				}
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(76);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TransitionContext extends ParserRuleContext {
		public ActionContext action() {
			return getRuleContext(ActionContext.class,0);
		}
		public Symbol_listContext symbol_list() {
			return getRuleContext(Symbol_listContext.class,0);
		}
		public TerminalNode SYMBOL() { return getToken(turingParser.SYMBOL, 0); }
		public TransitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transition; }
	}

	public final TransitionContext transition() throws RecognitionException {
		TransitionContext _localctx = new TransitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_transition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				{
				setState(78);
				symbol_list();
				}
				break;
			case SYMBOL:
				{
				setState(79);
				match(SYMBOL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(82);
			match(T__4);
			setState(83);
			action();
			setState(84);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ActionContext extends ParserRuleContext {
		public Move_actionContext move_action() {
			return getRuleContext(Move_actionContext.class,0);
		}
		public Write_actionContext write_action() {
			return getRuleContext(Write_actionContext.class,0);
		}
		public State_transitionContext state_transition() {
			return getRuleContext(State_transitionContext.class,0);
		}
		public ActionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_action; }
	}

	public final ActionContext action() throws RecognitionException {
		ActionContext _localctx = new ActionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_action);
		try {
			setState(100);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(86);
				move_action();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(87);
				match(T__9);
				setState(90);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__13:
					{
					setState(88);
					write_action();
					}
					break;
				case T__11:
				case T__12:
					{
					setState(89);
					state_transition();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(92);
				match(T__10);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(94);
				match(T__9);
				setState(95);
				write_action();
				setState(96);
				match(T__1);
				setState(97);
				state_transition();
				setState(98);
				match(T__10);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Move_actionContext extends ParserRuleContext {
		public Move_actionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_move_action; }
	}

	public final Move_actionContext move_action() throws RecognitionException {
		Move_actionContext _localctx = new Move_actionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_move_action);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			_la = _input.LA(1);
			if ( !(_la==T__11 || _la==T__12) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Write_actionContext extends ParserRuleContext {
		public TerminalNode SYMBOL() { return getToken(turingParser.SYMBOL, 0); }
		public Write_actionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_write_action; }
	}

	public final Write_actionContext write_action() throws RecognitionException {
		Write_actionContext _localctx = new Write_actionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_write_action);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(T__13);
			setState(105);
			match(T__4);
			setState(106);
			match(SYMBOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class State_transitionContext extends ParserRuleContext {
		public Move_actionContext move_action() {
			return getRuleContext(Move_actionContext.class,0);
		}
		public TerminalNode ID() { return getToken(turingParser.ID, 0); }
		public State_transitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_state_transition; }
	}

	public final State_transitionContext state_transition() throws RecognitionException {
		State_transitionContext _localctx = new State_transitionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_state_transition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			move_action();
			setState(111);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__4) {
				{
				setState(109);
				match(T__4);
				setState(110);
				match(ID);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\u0004\u0001\u0013r\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0005\u0001#\b\u0001\n\u0001"+
		"\f\u0001&\t\u0001\u0001\u0001\u0001\u0001\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0003\u0001\u0003\u0001\u0003\u0001"+
		"\u0003\u0001\u0003\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001"+
		"\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0005\u0005=\b"+
		"\u0005\n\u0005\f\u0005@\t\u0005\u0001\u0005\u0001\u0005\u0001\u0006\u0001"+
		"\u0006\u0001\u0006\u0001\u0006\u0005\u0006H\b\u0006\n\u0006\f\u0006K\t"+
		"\u0006\u0001\u0006\u0001\u0006\u0001\u0007\u0001\u0007\u0003\u0007Q\b"+
		"\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\b\u0001\b"+
		"\u0001\b\u0001\b\u0003\b[\b\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b"+
		"\u0001\b\u0001\b\u0001\b\u0003\be\b\b\u0001\t\u0001\t\u0001\n\u0001\n"+
		"\u0001\n\u0001\n\u0001\u000b\u0001\u000b\u0001\u000b\u0003\u000bp\b\u000b"+
		"\u0001\u000b\u0000\u0000\f\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012"+
		"\u0014\u0016\u0000\u0001\u0001\u0000\f\rm\u0000\u0018\u0001\u0000\u0000"+
		"\u0000\u0002\u001e\u0001\u0000\u0000\u0000\u0004)\u0001\u0000\u0000\u0000"+
		"\u0006.\u0001\u0000\u0000\u0000\b3\u0001\u0000\u0000\u0000\n8\u0001\u0000"+
		"\u0000\u0000\fC\u0001\u0000\u0000\u0000\u000eP\u0001\u0000\u0000\u0000"+
		"\u0010d\u0001\u0000\u0000\u0000\u0012f\u0001\u0000\u0000\u0000\u0014h"+
		"\u0001\u0000\u0000\u0000\u0016l\u0001\u0000\u0000\u0000\u0018\u0019\u0003"+
		"\u0004\u0002\u0000\u0019\u001a\u0003\u0006\u0003\u0000\u001a\u001b\u0003"+
		"\b\u0004\u0000\u001b\u001c\u0003\n\u0005\u0000\u001c\u001d\u0005\u0000"+
		"\u0000\u0001\u001d\u0001\u0001\u0000\u0000\u0000\u001e\u001f\u0005\u0001"+
		"\u0000\u0000\u001f$\u0005\u0010\u0000\u0000 !\u0005\u0002\u0000\u0000"+
		"!#\u0005\u0010\u0000\u0000\" \u0001\u0000\u0000\u0000#&\u0001\u0000\u0000"+
		"\u0000$\"\u0001\u0000\u0000\u0000$%\u0001\u0000\u0000\u0000%\'\u0001\u0000"+
		"\u0000\u0000&$\u0001\u0000\u0000\u0000\'(\u0005\u0003\u0000\u0000(\u0003"+
		"\u0001\u0000\u0000\u0000)*\u0005\u0004\u0000\u0000*+\u0005\u0005\u0000"+
		"\u0000+,\u0005\u000f\u0000\u0000,-\u0005\u0006\u0000\u0000-\u0005\u0001"+
		"\u0000\u0000\u0000./\u0005\u0007\u0000\u0000/0\u0005\u0005\u0000\u0000"+
		"01\u0005\u0010\u0000\u000012\u0005\u0006\u0000\u00002\u0007\u0001\u0000"+
		"\u0000\u000034\u0005\b\u0000\u000045\u0005\u0005\u0000\u000056\u0005\u0012"+
		"\u0000\u000067\u0005\u0006\u0000\u00007\t\u0001\u0000\u0000\u000089\u0005"+
		"\t\u0000\u00009:\u0005\u0005\u0000\u0000:>\u0005\n\u0000\u0000;=\u0003"+
		"\f\u0006\u0000<;\u0001\u0000\u0000\u0000=@\u0001\u0000\u0000\u0000><\u0001"+
		"\u0000\u0000\u0000>?\u0001\u0000\u0000\u0000?A\u0001\u0000\u0000\u0000"+
		"@>\u0001\u0000\u0000\u0000AB\u0005\u000b\u0000\u0000B\u000b\u0001\u0000"+
		"\u0000\u0000CD\u0005\u0012\u0000\u0000DE\u0005\u0005\u0000\u0000EI\u0005"+
		"\n\u0000\u0000FH\u0003\u000e\u0007\u0000GF\u0001\u0000\u0000\u0000HK\u0001"+
		"\u0000\u0000\u0000IG\u0001\u0000\u0000\u0000IJ\u0001\u0000\u0000\u0000"+
		"JL\u0001\u0000\u0000\u0000KI\u0001\u0000\u0000\u0000LM\u0005\u000b\u0000"+
		"\u0000M\r\u0001\u0000\u0000\u0000NQ\u0003\u0002\u0001\u0000OQ\u0005\u0010"+
		"\u0000\u0000PN\u0001\u0000\u0000\u0000PO\u0001\u0000\u0000\u0000QR\u0001"+
		"\u0000\u0000\u0000RS\u0005\u0005\u0000\u0000ST\u0003\u0010\b\u0000TU\u0005"+
		"\u0006\u0000\u0000U\u000f\u0001\u0000\u0000\u0000Ve\u0003\u0012\t\u0000"+
		"WZ\u0005\n\u0000\u0000X[\u0003\u0014\n\u0000Y[\u0003\u0016\u000b\u0000"+
		"ZX\u0001\u0000\u0000\u0000ZY\u0001\u0000\u0000\u0000[\\\u0001\u0000\u0000"+
		"\u0000\\]\u0005\u000b\u0000\u0000]e\u0001\u0000\u0000\u0000^_\u0005\n"+
		"\u0000\u0000_`\u0003\u0014\n\u0000`a\u0005\u0002\u0000\u0000ab\u0003\u0016"+
		"\u000b\u0000bc\u0005\u000b\u0000\u0000ce\u0001\u0000\u0000\u0000dV\u0001"+
		"\u0000\u0000\u0000dW\u0001\u0000\u0000\u0000d^\u0001\u0000\u0000\u0000"+
		"e\u0011\u0001\u0000\u0000\u0000fg\u0007\u0000\u0000\u0000g\u0013\u0001"+
		"\u0000\u0000\u0000hi\u0005\u000e\u0000\u0000ij\u0005\u0005\u0000\u0000"+
		"jk\u0005\u0010\u0000\u0000k\u0015\u0001\u0000\u0000\u0000lo\u0003\u0012"+
		"\t\u0000mn\u0005\u0005\u0000\u0000np\u0005\u0012\u0000\u0000om\u0001\u0000"+
		"\u0000\u0000op\u0001\u0000\u0000\u0000p\u0017\u0001\u0000\u0000\u0000"+
		"\u0007$>IPZdo";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}