# Generated from turing.g4 by ANTLR 4.13.1
from antlr4 import *
if "." in __name__:
    from .turingParser import turingParser
else:
    from turingParser import turingParser

# This class defines a complete listener for a parse tree produced by turingParser.
class turingListener(ParseTreeListener):

    # Enter a parse tree produced by turingParser#tm.
    def enterTm(self, ctx:turingParser.TmContext):
        pass

    # Exit a parse tree produced by turingParser#tm.
    def exitTm(self, ctx:turingParser.TmContext):
        pass


    # Enter a parse tree produced by turingParser#symbol_list.
    def enterSymbol_list(self, ctx:turingParser.Symbol_listContext):
        pass

    # Exit a parse tree produced by turingParser#symbol_list.
    def exitSymbol_list(self, ctx:turingParser.Symbol_listContext):
        pass


    # Enter a parse tree produced by turingParser#input_block.
    def enterInput_block(self, ctx:turingParser.Input_blockContext):
        pass

    # Exit a parse tree produced by turingParser#input_block.
    def exitInput_block(self, ctx:turingParser.Input_blockContext):
        pass


    # Enter a parse tree produced by turingParser#blank_block.
    def enterBlank_block(self, ctx:turingParser.Blank_blockContext):
        pass

    # Exit a parse tree produced by turingParser#blank_block.
    def exitBlank_block(self, ctx:turingParser.Blank_blockContext):
        pass


    # Enter a parse tree produced by turingParser#start_state_block.
    def enterStart_state_block(self, ctx:turingParser.Start_state_blockContext):
        pass

    # Exit a parse tree produced by turingParser#start_state_block.
    def exitStart_state_block(self, ctx:turingParser.Start_state_blockContext):
        pass


    # Enter a parse tree produced by turingParser#table_block.
    def enterTable_block(self, ctx:turingParser.Table_blockContext):
        pass

    # Exit a parse tree produced by turingParser#table_block.
    def exitTable_block(self, ctx:turingParser.Table_blockContext):
        pass


    # Enter a parse tree produced by turingParser#state_definition.
    def enterState_definition(self, ctx:turingParser.State_definitionContext):
        pass

    # Exit a parse tree produced by turingParser#state_definition.
    def exitState_definition(self, ctx:turingParser.State_definitionContext):
        pass


    # Enter a parse tree produced by turingParser#transition.
    def enterTransition(self, ctx:turingParser.TransitionContext):
        pass

    # Exit a parse tree produced by turingParser#transition.
    def exitTransition(self, ctx:turingParser.TransitionContext):
        pass


    # Enter a parse tree produced by turingParser#action.
    def enterAction(self, ctx:turingParser.ActionContext):
        pass

    # Exit a parse tree produced by turingParser#action.
    def exitAction(self, ctx:turingParser.ActionContext):
        pass


    # Enter a parse tree produced by turingParser#move_action.
    def enterMove_action(self, ctx:turingParser.Move_actionContext):
        pass

    # Exit a parse tree produced by turingParser#move_action.
    def exitMove_action(self, ctx:turingParser.Move_actionContext):
        pass


    # Enter a parse tree produced by turingParser#write_action.
    def enterWrite_action(self, ctx:turingParser.Write_actionContext):
        pass

    # Exit a parse tree produced by turingParser#write_action.
    def exitWrite_action(self, ctx:turingParser.Write_actionContext):
        pass


    # Enter a parse tree produced by turingParser#state_transition.
    def enterState_transition(self, ctx:turingParser.State_transitionContext):
        pass

    # Exit a parse tree produced by turingParser#state_transition.
    def exitState_transition(self, ctx:turingParser.State_transitionContext):
        pass



del turingParser