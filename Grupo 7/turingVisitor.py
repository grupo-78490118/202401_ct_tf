# Generated from turing.g4 by ANTLR 4.13.1
from antlr4 import *
if "." in __name__:
    from .turingParser import turingParser
else:
    from turingParser import turingParser

# This class defines a complete generic visitor for a parse tree produced by turingParser.

class turingVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by turingParser#tm.
    def visitTm(self, ctx:turingParser.TmContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#symbol_list.
    def visitSymbol_list(self, ctx:turingParser.Symbol_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#input_block.
    def visitInput_block(self, ctx:turingParser.Input_blockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#blank_block.
    def visitBlank_block(self, ctx:turingParser.Blank_blockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#start_state_block.
    def visitStart_state_block(self, ctx:turingParser.Start_state_blockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#table_block.
    def visitTable_block(self, ctx:turingParser.Table_blockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#state_definition.
    def visitState_definition(self, ctx:turingParser.State_definitionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#transition.
    def visitTransition(self, ctx:turingParser.TransitionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#action.
    def visitAction(self, ctx:turingParser.ActionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#move_action.
    def visitMove_action(self, ctx:turingParser.Move_actionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#write_action.
    def visitWrite_action(self, ctx:turingParser.Write_actionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by turingParser#state_transition.
    def visitState_transition(self, ctx:turingParser.State_transitionContext):
        return self.visitChildren(ctx)



del turingParser