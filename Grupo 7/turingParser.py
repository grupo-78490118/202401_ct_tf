# Generated from turing.g4 by ANTLR 4.13.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,19,114,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,1,0,1,0,1,0,1,0,1,
        0,1,0,1,1,1,1,1,1,1,1,5,1,35,8,1,10,1,12,1,38,9,1,1,1,1,1,1,2,1,
        2,1,2,1,2,1,2,1,3,1,3,1,3,1,3,1,3,1,4,1,4,1,4,1,4,1,4,1,5,1,5,1,
        5,1,5,5,5,61,8,5,10,5,12,5,64,9,5,1,5,1,5,1,6,1,6,1,6,1,6,5,6,72,
        8,6,10,6,12,6,75,9,6,1,6,1,6,1,7,1,7,3,7,81,8,7,1,7,1,7,1,7,1,7,
        1,8,1,8,1,8,1,8,3,8,91,8,8,1,8,1,8,1,8,1,8,1,8,1,8,1,8,1,8,3,8,101,
        8,8,1,9,1,9,1,10,1,10,1,10,1,10,1,11,1,11,1,11,3,11,112,8,11,1,11,
        0,0,12,0,2,4,6,8,10,12,14,16,18,20,22,0,1,1,0,12,13,109,0,24,1,0,
        0,0,2,30,1,0,0,0,4,41,1,0,0,0,6,46,1,0,0,0,8,51,1,0,0,0,10,56,1,
        0,0,0,12,67,1,0,0,0,14,80,1,0,0,0,16,100,1,0,0,0,18,102,1,0,0,0,
        20,104,1,0,0,0,22,108,1,0,0,0,24,25,3,4,2,0,25,26,3,6,3,0,26,27,
        3,8,4,0,27,28,3,10,5,0,28,29,5,0,0,1,29,1,1,0,0,0,30,31,5,1,0,0,
        31,36,5,16,0,0,32,33,5,2,0,0,33,35,5,16,0,0,34,32,1,0,0,0,35,38,
        1,0,0,0,36,34,1,0,0,0,36,37,1,0,0,0,37,39,1,0,0,0,38,36,1,0,0,0,
        39,40,5,3,0,0,40,3,1,0,0,0,41,42,5,4,0,0,42,43,5,5,0,0,43,44,5,15,
        0,0,44,45,5,6,0,0,45,5,1,0,0,0,46,47,5,7,0,0,47,48,5,5,0,0,48,49,
        5,16,0,0,49,50,5,6,0,0,50,7,1,0,0,0,51,52,5,8,0,0,52,53,5,5,0,0,
        53,54,5,18,0,0,54,55,5,6,0,0,55,9,1,0,0,0,56,57,5,9,0,0,57,58,5,
        5,0,0,58,62,5,10,0,0,59,61,3,12,6,0,60,59,1,0,0,0,61,64,1,0,0,0,
        62,60,1,0,0,0,62,63,1,0,0,0,63,65,1,0,0,0,64,62,1,0,0,0,65,66,5,
        11,0,0,66,11,1,0,0,0,67,68,5,18,0,0,68,69,5,5,0,0,69,73,5,10,0,0,
        70,72,3,14,7,0,71,70,1,0,0,0,72,75,1,0,0,0,73,71,1,0,0,0,73,74,1,
        0,0,0,74,76,1,0,0,0,75,73,1,0,0,0,76,77,5,11,0,0,77,13,1,0,0,0,78,
        81,3,2,1,0,79,81,5,16,0,0,80,78,1,0,0,0,80,79,1,0,0,0,81,82,1,0,
        0,0,82,83,5,5,0,0,83,84,3,16,8,0,84,85,5,6,0,0,85,15,1,0,0,0,86,
        101,3,18,9,0,87,90,5,10,0,0,88,91,3,20,10,0,89,91,3,22,11,0,90,88,
        1,0,0,0,90,89,1,0,0,0,91,92,1,0,0,0,92,93,5,11,0,0,93,101,1,0,0,
        0,94,95,5,10,0,0,95,96,3,20,10,0,96,97,5,2,0,0,97,98,3,22,11,0,98,
        99,5,11,0,0,99,101,1,0,0,0,100,86,1,0,0,0,100,87,1,0,0,0,100,94,
        1,0,0,0,101,17,1,0,0,0,102,103,7,0,0,0,103,19,1,0,0,0,104,105,5,
        14,0,0,105,106,5,5,0,0,106,107,5,16,0,0,107,21,1,0,0,0,108,111,3,
        18,9,0,109,110,5,5,0,0,110,112,5,18,0,0,111,109,1,0,0,0,111,112,
        1,0,0,0,112,23,1,0,0,0,7,36,62,73,80,90,100,111
    ]

class turingParser ( Parser ):

    grammarFileName = "turing.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'['", "','", "']'", "'input'", "':'", 
                     "';'", "'blank'", "'start_state'", "'table'", "'{'", 
                     "'}'", "'L'", "'R'", "'write'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "STRING", "SYMBOL", 
                      "INT", "ID", "WS" ]

    RULE_tm = 0
    RULE_symbol_list = 1
    RULE_input_block = 2
    RULE_blank_block = 3
    RULE_start_state_block = 4
    RULE_table_block = 5
    RULE_state_definition = 6
    RULE_transition = 7
    RULE_action = 8
    RULE_move_action = 9
    RULE_write_action = 10
    RULE_state_transition = 11

    ruleNames =  [ "tm", "symbol_list", "input_block", "blank_block", "start_state_block", 
                   "table_block", "state_definition", "transition", "action", 
                   "move_action", "write_action", "state_transition" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    STRING=15
    SYMBOL=16
    INT=17
    ID=18
    WS=19

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class TmContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def input_block(self):
            return self.getTypedRuleContext(turingParser.Input_blockContext,0)


        def blank_block(self):
            return self.getTypedRuleContext(turingParser.Blank_blockContext,0)


        def start_state_block(self):
            return self.getTypedRuleContext(turingParser.Start_state_blockContext,0)


        def table_block(self):
            return self.getTypedRuleContext(turingParser.Table_blockContext,0)


        def EOF(self):
            return self.getToken(turingParser.EOF, 0)

        def getRuleIndex(self):
            return turingParser.RULE_tm

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTm" ):
                listener.enterTm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTm" ):
                listener.exitTm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTm" ):
                return visitor.visitTm(self)
            else:
                return visitor.visitChildren(self)




    def tm(self):

        localctx = turingParser.TmContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_tm)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            self.input_block()
            self.state = 25
            self.blank_block()
            self.state = 26
            self.start_state_block()
            self.state = 27
            self.table_block()
            self.state = 28
            self.match(turingParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Symbol_listContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMBOL(self, i:int=None):
            if i is None:
                return self.getTokens(turingParser.SYMBOL)
            else:
                return self.getToken(turingParser.SYMBOL, i)

        def getRuleIndex(self):
            return turingParser.RULE_symbol_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbol_list" ):
                listener.enterSymbol_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbol_list" ):
                listener.exitSymbol_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymbol_list" ):
                return visitor.visitSymbol_list(self)
            else:
                return visitor.visitChildren(self)




    def symbol_list(self):

        localctx = turingParser.Symbol_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_symbol_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 30
            self.match(turingParser.T__0)
            self.state = 31
            self.match(turingParser.SYMBOL)
            self.state = 36
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==2:
                self.state = 32
                self.match(turingParser.T__1)
                self.state = 33
                self.match(turingParser.SYMBOL)
                self.state = 38
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 39
            self.match(turingParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Input_blockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self):
            return self.getToken(turingParser.STRING, 0)

        def getRuleIndex(self):
            return turingParser.RULE_input_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInput_block" ):
                listener.enterInput_block(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInput_block" ):
                listener.exitInput_block(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInput_block" ):
                return visitor.visitInput_block(self)
            else:
                return visitor.visitChildren(self)




    def input_block(self):

        localctx = turingParser.Input_blockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_input_block)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self.match(turingParser.T__3)
            self.state = 42
            self.match(turingParser.T__4)
            self.state = 43
            self.match(turingParser.STRING)
            self.state = 44
            self.match(turingParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Blank_blockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMBOL(self):
            return self.getToken(turingParser.SYMBOL, 0)

        def getRuleIndex(self):
            return turingParser.RULE_blank_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlank_block" ):
                listener.enterBlank_block(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlank_block" ):
                listener.exitBlank_block(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlank_block" ):
                return visitor.visitBlank_block(self)
            else:
                return visitor.visitChildren(self)




    def blank_block(self):

        localctx = turingParser.Blank_blockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_blank_block)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            self.match(turingParser.T__6)
            self.state = 47
            self.match(turingParser.T__4)
            self.state = 48
            self.match(turingParser.SYMBOL)
            self.state = 49
            self.match(turingParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Start_state_blockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(turingParser.ID, 0)

        def getRuleIndex(self):
            return turingParser.RULE_start_state_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart_state_block" ):
                listener.enterStart_state_block(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart_state_block" ):
                listener.exitStart_state_block(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart_state_block" ):
                return visitor.visitStart_state_block(self)
            else:
                return visitor.visitChildren(self)




    def start_state_block(self):

        localctx = turingParser.Start_state_blockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_start_state_block)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 51
            self.match(turingParser.T__7)
            self.state = 52
            self.match(turingParser.T__4)
            self.state = 53
            self.match(turingParser.ID)
            self.state = 54
            self.match(turingParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Table_blockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def state_definition(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(turingParser.State_definitionContext)
            else:
                return self.getTypedRuleContext(turingParser.State_definitionContext,i)


        def getRuleIndex(self):
            return turingParser.RULE_table_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTable_block" ):
                listener.enterTable_block(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTable_block" ):
                listener.exitTable_block(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTable_block" ):
                return visitor.visitTable_block(self)
            else:
                return visitor.visitChildren(self)




    def table_block(self):

        localctx = turingParser.Table_blockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_table_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 56
            self.match(turingParser.T__8)
            self.state = 57
            self.match(turingParser.T__4)
            self.state = 58
            self.match(turingParser.T__9)
            self.state = 62
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==18:
                self.state = 59
                self.state_definition()
                self.state = 64
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 65
            self.match(turingParser.T__10)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class State_definitionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(turingParser.ID, 0)

        def transition(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(turingParser.TransitionContext)
            else:
                return self.getTypedRuleContext(turingParser.TransitionContext,i)


        def getRuleIndex(self):
            return turingParser.RULE_state_definition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterState_definition" ):
                listener.enterState_definition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitState_definition" ):
                listener.exitState_definition(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitState_definition" ):
                return visitor.visitState_definition(self)
            else:
                return visitor.visitChildren(self)




    def state_definition(self):

        localctx = turingParser.State_definitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_state_definition)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 67
            self.match(turingParser.ID)
            self.state = 68
            self.match(turingParser.T__4)
            self.state = 69
            self.match(turingParser.T__9)
            self.state = 73
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==1 or _la==16:
                self.state = 70
                self.transition()
                self.state = 75
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 76
            self.match(turingParser.T__10)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TransitionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def action(self):
            return self.getTypedRuleContext(turingParser.ActionContext,0)


        def symbol_list(self):
            return self.getTypedRuleContext(turingParser.Symbol_listContext,0)


        def SYMBOL(self):
            return self.getToken(turingParser.SYMBOL, 0)

        def getRuleIndex(self):
            return turingParser.RULE_transition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTransition" ):
                listener.enterTransition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTransition" ):
                listener.exitTransition(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTransition" ):
                return visitor.visitTransition(self)
            else:
                return visitor.visitChildren(self)




    def transition(self):

        localctx = turingParser.TransitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_transition)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1]:
                self.state = 78
                self.symbol_list()
                pass
            elif token in [16]:
                self.state = 79
                self.match(turingParser.SYMBOL)
                pass
            else:
                raise NoViableAltException(self)

            self.state = 82
            self.match(turingParser.T__4)
            self.state = 83
            self.action()
            self.state = 84
            self.match(turingParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ActionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def move_action(self):
            return self.getTypedRuleContext(turingParser.Move_actionContext,0)


        def write_action(self):
            return self.getTypedRuleContext(turingParser.Write_actionContext,0)


        def state_transition(self):
            return self.getTypedRuleContext(turingParser.State_transitionContext,0)


        def getRuleIndex(self):
            return turingParser.RULE_action

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAction" ):
                listener.enterAction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAction" ):
                listener.exitAction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAction" ):
                return visitor.visitAction(self)
            else:
                return visitor.visitChildren(self)




    def action(self):

        localctx = turingParser.ActionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_action)
        try:
            self.state = 100
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 86
                self.move_action()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 87
                self.match(turingParser.T__9)
                self.state = 90
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [14]:
                    self.state = 88
                    self.write_action()
                    pass
                elif token in [12, 13]:
                    self.state = 89
                    self.state_transition()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 92
                self.match(turingParser.T__10)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 94
                self.match(turingParser.T__9)
                self.state = 95
                self.write_action()
                self.state = 96
                self.match(turingParser.T__1)
                self.state = 97
                self.state_transition()
                self.state = 98
                self.match(turingParser.T__10)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Move_actionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return turingParser.RULE_move_action

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMove_action" ):
                listener.enterMove_action(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMove_action" ):
                listener.exitMove_action(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMove_action" ):
                return visitor.visitMove_action(self)
            else:
                return visitor.visitChildren(self)




    def move_action(self):

        localctx = turingParser.Move_actionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_move_action)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            _la = self._input.LA(1)
            if not(_la==12 or _la==13):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Write_actionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMBOL(self):
            return self.getToken(turingParser.SYMBOL, 0)

        def getRuleIndex(self):
            return turingParser.RULE_write_action

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWrite_action" ):
                listener.enterWrite_action(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWrite_action" ):
                listener.exitWrite_action(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWrite_action" ):
                return visitor.visitWrite_action(self)
            else:
                return visitor.visitChildren(self)




    def write_action(self):

        localctx = turingParser.Write_actionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_write_action)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 104
            self.match(turingParser.T__13)
            self.state = 105
            self.match(turingParser.T__4)
            self.state = 106
            self.match(turingParser.SYMBOL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class State_transitionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def move_action(self):
            return self.getTypedRuleContext(turingParser.Move_actionContext,0)


        def ID(self):
            return self.getToken(turingParser.ID, 0)

        def getRuleIndex(self):
            return turingParser.RULE_state_transition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterState_transition" ):
                listener.enterState_transition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitState_transition" ):
                listener.exitState_transition(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitState_transition" ):
                return visitor.visitState_transition(self)
            else:
                return visitor.visitChildren(self)




    def state_transition(self):

        localctx = turingParser.State_transitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_state_transition)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            self.move_action()
            self.state = 111
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==5:
                self.state = 109
                self.match(turingParser.T__4)
                self.state = 110
                self.match(turingParser.ID)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





