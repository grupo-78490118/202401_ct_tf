grammar turing;



STRING: '\'' [a-zA-Z0-9 ][a-zA-Z0-9 ]+ '\'';
SYMBOL: '\'' . '\'' |[0-9];
INT : [0-9]+ ;
ID: [a-zA-Z_][a-zA-Z_0-9]* ;
WS: [ \t\n\r\f]+ -> skip ;


tm: input_block blank_block start_state_block table_block EOF;

symbol_list: '['SYMBOL(','SYMBOL)*']';

input_block: 'input' ':' STRING ';';
blank_block: 'blank' ':' SYMBOL';';
start_state_block: 'start_state'':'ID';';
table_block: 'table' ':' '{' state_definition*'}';

state_definition: ID':''{'transition*'}';
transition: (symbol_list | SYMBOL) ':' action ';';

action     : move_action |'{'(write_action | state_transition)'}' 
| '{'write_action',' state_transition'}';

move_action: ('L' | 'R');
write_action: 'write' ':' SYMBOL;
state_transition: move_action (':' ID)?;




